function sleep(ms: number): Promise<any> {
  return new Promise((resolve: any): void => {
    setTimeout(resolve, ms);
  });
}

const onKill = async (ctx: any, _msg: any) => {
  ctx.Event.emit('log', 'Sink stopping: auth_sink');
  await sleep(2000);
  process.exit();
}

export default onKill;
