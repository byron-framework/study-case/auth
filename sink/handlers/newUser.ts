const newUser = async (ctx: any, msg: any): Promise<void> => {
  const data: any = JSON.parse(msg.getData());
  await ctx.db.User.create({ ...data });
};

export default newUser;
