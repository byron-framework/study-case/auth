const createUser = async (_p: any, args: any, ctx: any, _i: any): Promise<any> => {
  const { db: { User }, bcrypt, Error, Event, uuidv4 }: any = ctx;
  const { name, email, password, passwordConfirmation }: any = args.data;

  let user = await User.find({ email });
  
  console.log(`returned from db: [${user}]`);

  if (user.length > 0) {
    throw new Error('user already exists', '409');
  }

  if (password !== passwordConfirmation) {
    throw new Error('passwords don\'t match');
  }

  user = new User({
    _id: uuidv4(),
    name,
    email,
    password: bcrypt.hashSync(password)
  });

  Event.emit('new.user', user);

  return user;
};

export default createUser;
