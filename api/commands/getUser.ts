const getUser = async (_p: any, args: any, ctx: any, _i: any): Promise<any> => {
  const { db: { User }, Error }: any = ctx;
  const { id }: any = args;

  const user: any = await User.findById(id);

  if (!user) {
    throw new Error('user doesn\'t exist', '409');
  }

  return user;
};

export default getUser;
