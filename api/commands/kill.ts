function sleep(ms: number): Promise<any> {
  return new Promise((resolve: any): void => {
    setTimeout(resolve, ms);
  });
}


const kill = async (_p: any, _args: any, ctx: any, _i: any) => {
  ctx.Event.emit('auth.kill');
  await sleep(1000);
  process.exit();
}

export default kill;
